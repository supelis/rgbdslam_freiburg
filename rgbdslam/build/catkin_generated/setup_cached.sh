#!/usr/bin/env sh
# generated from catkin/python/catkin/environment_cache.py

# based on a snapshot of the environment before and after calling the setup script
# it emulates the modifications of the setup script without recurring computations

# new environment variables

# modified environment variables
export CATKIN_TEST_RESULTS_DIR="/home/rokas/catkin_ws/src/rgbdslam_freiburg/rgbdslam/build/test_results"
export CMAKE_PREFIX_PATH="/home/rokas/catkin_ws/src/rgbdslam_freiburg/rgbdslam/build/devel:$CMAKE_PREFIX_PATH"
export CPATH="/home/rokas/catkin_ws/src/rgbdslam_freiburg/rgbdslam/build/devel/include:$CPATH"
export LD_LIBRARY_PATH="/home/rokas/catkin_ws/src/rgbdslam_freiburg/rgbdslam/build/devel/lib:$LD_LIBRARY_PATH"
export PATH="/home/rokas/catkin_ws/src/rgbdslam_freiburg/rgbdslam/build/devel/bin:$PATH"
export PKG_CONFIG_PATH="/home/rokas/catkin_ws/src/rgbdslam_freiburg/rgbdslam/build/devel/lib/pkgconfig:$PKG_CONFIG_PATH"
export PYTHONPATH="/home/rokas/catkin_ws/src/rgbdslam_freiburg/rgbdslam/build/devel/lib/python2.7/dist-packages:$PYTHONPATH"
export ROSLISP_PACKAGE_DIRECTORIES="/home/rokas/catkin_ws/src/rgbdslam_freiburg/rgbdslam/build/devel/share/common-lisp:$ROSLISP_PACKAGE_DIRECTORIES"
export ROS_PACKAGE_PATH="/home/rokas/catkin_ws/src/rgbdslam_freiburg/rgbdslam:/home/rokas/hector_quadrotor_tutorial/src:/home/rokas/catkin_ws/quadrotor_controller/src:/home/rokas/catkin_ws/src:/opt/ros/hydro/share:/opt/ros/hydro/stacks"
export ROS_TEST_RESULTS_DIR="/home/rokas/catkin_ws/src/rgbdslam_freiburg/rgbdslam/build/test_results"